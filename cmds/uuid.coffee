_ids = {}

S4 = -> ((1 + Math.random()) * 0x10000 | 0).toString(16).substring 1
getId = -> S4()+S4()+'-'+S4()+'-'+S4()+'-'+S4()+'-'+S4()+S4()+S4()
uuid = -> id = getId(); id = getId() while _ids[id]; _ids[id] = id

module.exports =
  ID: 'UUID'
  NAME: 'UUID'
  PROC: -> uuid()