module.exports =
  ID: 'ADD'
  NAME: 'Add'
  DESC: 'Adds two given values together'
  VALS: ['val1','val2','dest']
  CHECK: (step) -> !!step.val1 && !!step.val2 && !!step.dest
  PROC: (step) -> (parseInt(step.val1)||0) + (parseInt(step.val2)||0)