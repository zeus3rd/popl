_commands = {}
_programs = {}

# Piggy back uuid from uuid cmd file
uuid = require './cmds/uuid'

PASS = -> true

# Validates given scripting object to ensure it is compatible with POPL
validate_command = (obj, done) ->
  if !obj.ID then return console.error 'Error > Missing ID', obj
  if _commands[obj.ID] then return console.error 'Error > Attempted to define function '+obj.ID+' twice'
  if !obj.NAME then return console.error 'Error > Missing NAME string for', obj
  if !obj.DESC then console.warn 'Warning > No DESC (Description) has been given for', obj
  if !obj.PROC then return console.error 'Error > Missing PROC function for', obj
  if !obj.CHECK then return console.error 'Error > Missing CHECK function for', obj
  if !(obj.VALS && obj.VALS.length) then return console.warn 'Warning > No VALS set for', obj
  done()

validate_step = (step,data) -> step.CMD? && _commands[step.CMD]? && (_commands[step.CMD].CHECK||PASS)?(step,data)
execute_step = (step,data) -> _commands[step.CMD].PROC step
validate_program = (prog) ->
  # Check that prog.steps is an array with steps (empty arrays are not acceptable)
  if !prog then return !! console.error 'No program'
  if !(typeof prog.steps is 'object') then return !! console.error 'Error > prog.steps must be an array'
  if !(prog.steps.length) then return !! console.error 'Error > prog.steps must be an array of steps'
  true

module.exports = popl =
  # Storing and viewing commands
  command:
    include: (obj) -> validate_command obj, -> _commands[obj.ID] = obj
    list: -> Object.keys _commands
    view: (id) -> _commands[id]

  # Storing, viewing and executing programs
  program:
    # Execute program (given or stored)
    exec: (prog,data) ->
      if typeof prog == 'string' then prog = _programs[prog]

      # Validate Program
      if !prog then return !! console.error 'Error > Execute nothing?'
      if !validate_program prog then return !! console.error 'Error > Program Aborted'

      # Run each step in program
      for (i,step) in prog.steps
        if validate_step step then execute_step step, data else return !! console.error "Error > Step #{i} > invalid step"

      # Return data object for daisy chaining processes
      data

    store: (prog,id) ->
      id = id || uuid.PROC()
      if _programs[id] then return console.error 'Error > Attempted to define program '+id+' twice'
      _programs[id] = prog
      return id

    # Validates a program structure without running it
    validate: (prog) -> validate_program prog

  # TODO: Command triggers


# Load default commands
popl.command.include uuid
popl.command.include require './cmds/add'