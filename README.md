
# POPL
*the Possum's Own Programming Language*

This is an experimental project to build a Browser GUI orientated programming language which is completely customizable from server to server. It allows users to create, test, validate and run JSON-structure programs within their own browsers in an idental way

## Coffeescript

All examples are written in coffeescript because the little possum loves little code

## Installation

*Until popl is registered on npm, install from the gitlab repo directly*

```
npm i --save https://gitlab.com/zeus3rd/popl.git
```

## Implementation

Create a common inclusion file where you add any of your custom commands

```
popl = require 'popl'

# Your custom script(s)
# popl.command.include require './cmds/myCmd.coffee'

module.exports = popl
```

now ```require()``` this file within your express server and/or webpack client code builder so all instances use your defined commands in the same way

## Commands

### popl.commands.list

Returns an array of command IDs you have included in popl. This is intended to be used in your client program editor

```
popl.commands.list()
# ['ADD','UUID', ... ]
```

### popl.commands.view

Fetches the internal command object in order to access NAME, DESC and all other details about a command for user to reference in your UI (described below)

```
popl.commands.view 'ADD'
# Returns command object
```

### popl.commands.include

#### Creating Custom Commands

*cmds/myCmd.coffee*

```
myCmd =
  ID: 'justdoit'
  NAME: 'Just Do It'
  DESC: 'Tells you to JUST DO IT'
  VALS: ['dest']
  CHECK: (step) -> !!step.dest
  PROC: (step, data) -> data[step.dest] = 'JUST DO IT!'

module.exports = myCmd
```

*popl.plugin.coffee*

```
popl = require 'popl'
popl.include require './cmds/myCmd'
module.exports = popl
```

##### ID
Format: String
The unique ID for your command, this will not be visible to your users but changing this will cause old programs using your command to fail when updated

##### NAME
Format: String
A display name for your command to be shown to user in an interface/dropdown

##### DESC
Format: String
A human-readable description so users understand what your command is and how it works

##### VALS
Format: Array
An array of values keys which CAN be set (you should include all required AND optional values), you should list these values in the order a user would expect to set them so dynamic interfaces

##### CHECK
_optional_
Format: Function
Parameters: step
The function called to validate that a command step in a program has been formatted correctly to your and your command's expectations. Return truthy or falsey value to pass or fail this validation check. You may console.error() to tell the user exactly what they've done wrong and why their program has failed to run

##### PROC
Format: Function
Parameters: step, data
Your function called by a user's program

## Programs

### popl.program.exec

*TODO:*

### popl.program.store

*TODO:*

### popl.program.validate

*TODO:*
